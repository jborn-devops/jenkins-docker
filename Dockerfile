FROM jenkins/jenkins:2.249.3-lts-alpine

USER root

RUN apk update && apk add --no-cache --update docker openrc
RUN rc-update add docker boot